<?php

    //domba
    require_once('animal.php');

    $sheep = new animal("shaun");

    echo "Name : " . $sheep->Name . "<br>";
    echo "legs : " . $sheep->legs . "<br>";
    echo "cold blooded : " . $sheep->cold_blooded . "<br>";
    echo "<br>";

    //kodok
    require_once('frog.php');

    $kodok = new frog("buduk");

    echo "Name : " . $kodok->Name . "<br>";
    echo "legs : " . $kodok->legs . "<br>";
    echo "cold blooded : " . $kodok->cold_blooded . "<br>";
    echo "Jump : " . $kodok->Jump . "<br>";
    echo "<br>";

    //kera
    require_once('Ape.php');

    $sungokong = new Ape("kera sakti");

    echo "Name : " . $sungokong->Name . "<br>";
    echo "legs : " . $sungokong->legs . "<br>";
    echo "cold blooded : " . $sungokong->cold_blooded . "<br>";
    echo "Yell : " . $sungokong->Yell . "<br>";
    echo "<br>";

?>